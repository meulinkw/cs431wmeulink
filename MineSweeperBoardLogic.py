import numpy
import math

class MineSweeperBoardLogic:
    
    emptySpots = 0
    
    def __init__(self,fileName):
        fileName = open("boards/" + fileName)
        self.fileString = fileName.read()
        #strip leading white space
        self.fileString = self.fileString.lstrip()
        
        #populateBoard(0):  - means empty space
        #populateBoard(1):  -1 means empty space
        if "- " in self.fileString:
            self.populateBoard(1)
        else:
            self.populateBoard(0)
    
    
    def violatesRules(self, hashMap, canadate):
        grid = self.solutionCanadate(hashMap, canadate)
        i = 0
        j = 0
        valid = True
        while i < self.rowSize and valid:
            while j < self.numColumns and valid:
                if not self.boardArray[i][j] == -1 and not self.bombsAt( i, j, grid) <= self.boardArray[i][j]:
                    valid = False  
                j = j + 1
            j = 0
            i = i + 1
        return not valid
            
    def solutionCanadate(self, hashMap, canadate):
        grid = numpy.copy(self.boardArray)
        index = 0
        while index < self.emptySpots:
            i = math.floor(hashMap.mapSet[index] / self.numColumns)
            j = hashMap.mapSet[index] % self.numColumns
            #-2 means bomb, -1 means no bomb
            if canadate[index] == 1:
                grid[i][j] = -2
            index = index + 1
        return grid
        
    def testPotentialSolution(self, hashMap, canadate):
        grid = self.solutionCanadate(hashMap, canadate)

        i = 0
        j = 0
        valid = True
        while i < self.rowSize and valid:
            while j < self.numColumns and valid:
                if not self.boardArray[i][j] == -1 and not self.bombsAt(i, j, grid) == self.boardArray[i][j]:
                    valid = False  
                j = j + 1
            j = 0
            i = i + 1
        return valid
            
    def bombsAt(self, i, j, grid):
        if grid[i][j] < 0:
            return 0
            
        numBombs = 0
        if i == 0:
            if j == 0:
                j = j + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                i = i + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                j = j - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                return numBombs
                    
            if j == self.numColumns - 1:
                j = j - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                i = i + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                j = j + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                return numBombs
                
            j = j + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            return numBombs
                
        if i == self.rowSize - 1:
            if j == 0:
                j = j + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                i = i - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                j = j - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                return numBombs
                    
            if j == self.numColumns - 1:
                j = j - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                i = i - 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                j = j + 1
                if grid[i][j] == -2:
                    numBombs = numBombs + 1
                return numBombs
                
            j = j + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            return numBombs
                
        if j == 0:
            j = j + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i + 2
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            return numBombs
            
        if j == self.numColumns - 1:
            j = j - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i - 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            j = j + 1
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            i = i + 2
            if grid[i][j] == -2:
                numBombs = numBombs + 1
            return numBombs
                
        j = j + 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        i = i - 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        j = j - 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        j = j - 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        i = i + 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        i = i + 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        j = j + 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
        j = j + 1
        if grid[i][j] == -2:
            numBombs = numBombs + 1
            
        return numBombs
            
            
            
                
                    
                
    def populateBoard(self, boardType):
        i = 0
        rowString = ""
        colString =""
        
        while not self.fileString[i].isspace():
            rowString += self.fileString[i]
            i = i + 1
        self.rowSize = int(rowString)
        
        while self.fileString[i].isspace():
            i = i + 1
        
        while not self.fileString[i].isspace():
            colString += self.fileString[i]
            i = i + 1
        self.numColumns = int(colString)
        
        while self.fileString[i].isspace():
            i = i + 1
        
        self.boardArray = numpy.zeros((self.rowSize, self.numColumns))
        curRow = 0
        curCol = 0
        while curRow < self.rowSize:
            while curCol < self.numColumns:
                if self.fileString[i].isspace():
                    i = i + 1
                if self.fileString[i] == "-":
                    if boardType == 1:
                        self.boardArray[curRow][curCol] = -1
                        self.emptySpots += 1
                        i = i + 1
                    if boardType == 0:
                        self.boardArray[curRow][curCol] = -1
                        self.emptySpots += 1
                        i += 2
                    curCol = curCol + 1
                else:
                    self.boardArray[curRow][curCol] = self.fileString[i]
                    i = i + 1
                    curCol = curCol + 1
            curCol = 0
            curRow = curRow + 1
            
