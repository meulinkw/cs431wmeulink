import numpy
from MineSweeperBoardLogic import MineSweeperBoardLogic

class MyMap:
    def __init__(self, board):
        self.mapSet = numpy.zeros(board.emptySpots)
        mapIndex = 0
        curRow = 0
        curCol = 0
        while mapIndex < board.emptySpots:
            while curRow < board.rowSize:
                while curCol < board.numColumns:
                    if board.boardArray[curRow][curCol] == -1:
                        self.mapSet[mapIndex] = curRow * board.numColumns + curCol
                        mapIndex = mapIndex + 1
                    curCol = curCol + 1
                curRow = curRow + 1
                curCol = 0
