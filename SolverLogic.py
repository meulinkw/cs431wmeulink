from MineSweeperBoardLogic import MineSweeperBoardLogic
from MyMap import MyMap
from Stack import Stack
import numpy

progressReport = True

def next(minesweeper, potential, dataStructure):
    i = 0
    while i < minesweeper.emptySpots:
        nextCanadate = numpy.copy(potential)
        if nextCanadate[i] == 1:
            return
        nextCanadate[i] = 1
        dataStructure.push(nextCanadate)
        i = i + 1

        
print("File name: ")
fileName = input()
minesweeper = MineSweeperBoardLogic(fileName)

hashMap = MyMap(minesweeper)
numSolutions = 0
dataStructure = Stack()
initial = numpy.zeros(minesweeper.emptySpots)


dataStructure.push(initial)

count = 0
while not dataStructure.isEmpty():
    potential = dataStructure.pop()
    count = count + 1
    if progressReport:
        print("Currently processing heristic number: " + str(count))
        print(potential)
    if minesweeper.testPotentialSolution(hashMap, potential):
        solution = minesweeper.solutionCanadate(hashMap, potential)
        numSolutions = numSolutions + 1
    if index < minesweeper.emptySpots:
        next(minesweeper, potential, dataStructure)

print()
print("-1 == no bomb and -2 == bomb")
print()
print("# of Solutions: " + str(numSolutions))
if numSolutions > 1:
    print("Need to Guess?: Yes, since there is more than one solution.")
if numSolutions == 1:
    print(solution)
print()
