class Stack:
    def __init__(self):
        self.items = []
        self.count = 0
        
    def isEmpty(self):
        if self.count == 0:
            return True
        else:
            return False
            
    def push(self, item):
        self.items.append(item)
        self.count = self.count + 1
        
    def pop(self):
        self.count = self.count - 1
        return self.items.pop()
        
    
    def peek(self):
        return self.items[count - 1]

    def size(self):
        return self.count